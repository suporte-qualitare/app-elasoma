import React, { useContext, Fragment } from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import { useSelector } from 'react-redux';
import CheckBox from '@react-native-community/checkbox';
import { Formik, Field } from 'formik';
import * as Yup from 'yup';
import { ThemeContext } from 'styled-components';
import { useNavigation } from '@react-navigation/native';

import FormItem from '../../components/Form/FormItem';
import Input from '../../components/Input';
import Text from '../../components/Typography/Text';
import Button from '../../components/Buttons/Button';
import SwitchForm from '../../components/Form/SwitchForm';
import MaskedInput from '../../components/MaskedInput';
import StateSelector from './StateSelector';
import SegmentSelector from './SegmentSelector';
import CitySelector from './CitySelector';
import { RegistrationSchema } from '../../contants/formSchemas';

export default ({ onSubmit, loading }) => {
  const themeContext = useContext(ThemeContext);
  const { user } = useSelector(state => state.auth);
  const navigation = useNavigation();

  const canEditCpf = !user.cpf;

  return (
    <Formik
      validationSchema={RegistrationSchema}
      initialValues={user}
      onSubmit={(values) => onSubmit(values)}>
      {({
        handleChange,
        handleBlur,
        setFieldTouched,
        handleSubmit,
        values,
        setFieldValue,
        errors,
        touched,
      }) => (
        <Fragment>
          <FormItem>
            <Input
              style={{ color: themeContext.drawer.textProfileColor }}
              placeholder="Nome Completo"
              onChangeText={handleChange('name')}
              onBlur={() => setFieldTouched('name')}
              value={values.name}
              error={touched.name && errors.name}
            />
          </FormItem>

          <FormItem>
            <Input
              style={{ color: themeContext.drawer.textProfileColor }}
              editable={false}
              placeholder="Email"
              value={values.email}
              autoCapitalize="none"
            />
          </FormItem>

          <FormItem>
            <MaskedInput
              mask={'[000].[000].[000]-[00]'}
              editable={canEditCpf}
              keyboardType="decimal-pad"
              placeholder="CPF"
              onChangeText={handleChange('cpf')}
              onBlur={handleBlur('cpf')}
              style={{ color: themeContext.drawer.textProfileColor }}
              value={values.cpf}
              error={touched.cpf && errors.cpf}
            />
          </FormItem>

          <FormItem>
            <MaskedInput
              mask={'([00]) [00000]-[0000]'}
              keyboardType="decimal-pad"
              placeholder="Celular"
              onChangeText={handleChange('cellphone')}
              onBlur={handleBlur('cellphone')}
              style={{ color: themeContext.drawer.textProfileColor }}
              value={values.cellphone}
              error={touched.cellphone && errors.cellphone}
            />
          </FormItem>

          <FormItem>
            <SegmentSelector
              selected={values.segment}
              onValueChange={e => setFieldValue('segment', e)}
              error={touched.segment && errors.segment}
            />
          </FormItem>

          <FormItem>
            <StateSelector
              selected={values.state}
              onValueChange={e => setFieldValue('state', e)}
              error={touched.state && errors.state}
            />
          </FormItem>

          <FormItem>
            <CitySelector
              selected={values.city}
              stateId={values.state}
              onValueChange={handleChange('city')}
              error={touched.city && errors.city}
            />
          </FormItem>

          <FormItem>
            <MaskedInput
              mask={'[00].[000].[000]/[0000]-[00]'}
              keyboardType="decimal-pad"
              placeholder="CNPJ"
              onChangeText={handleChange('cnpj')}
              onBlur={handleBlur('cnpj')}
              style={{ color: themeContext.drawer.textProfileColor }}
              value={values.cnpj}
              error={touched.cnpj && errors.cnpj}
            />
          </FormItem>

          <FormItem>
            <Input
              placeholder="Empresa"
              onChangeText={handleChange('business')}
              onBlur={handleBlur('business')}
              style={{ color: themeContext.drawer.textProfileColor }}
              value={values.business}
              error={touched.business && errors.business}
            />
          </FormItem>

          <FormItem>
            <MaskedInput
              mask={'([00]) [00000]-[0000]'}
              keyboardType="decimal-pad"
              placeholder="Whatsapp"
              onChangeText={handleChange('phone')}
              onBlur={handleBlur('phone')}
              style={{ color: themeContext.drawer.textProfileColor }}
              value={values.phone}
              error={touched.phone && errors.phone}
            />
            <SwitchForm
              label="Deixar whatsapp público"
              labelStyle={{ color: themeContext.drawer.textProfileColor }}
              value={values.public_phone}
              onValueChange={value => setFieldValue('public_phone', value)}
            />
          </FormItem>

          <FormItem>
            <Input
              placeholder="Instagram"
              onChangeText={handleChange('instagram')}
              onBlur={handleBlur('instagram')}
              style={{ color: themeContext.drawer.textProfileColor }}
              value={values.instagram}
            />
            <SwitchForm
              label="Deixar instagram público"
              labelStyle={{ color: themeContext.drawer.textProfileColor }}
              value={values.public_instagram}
              onValueChange={value => setFieldValue('public_instagram', value)}
            />
          </FormItem>

          <FormItem>
            <Input
              keyboardType="decimal-pad"
              placeholder="Quantidade de Funcionários"
              onChangeText={handleChange('employees')}
              onBlur={handleBlur('employees')}
              style={{ color: themeContext.drawer.textProfileColor }}
              value={values.employees}
              error={touched.employees && errors.employees}
            />
          </FormItem>

          <FormItem>
            <Input
              keyboardType="decimal-pad"
              placeholder="Média de faturamento anual do seu negócio"
              onChangeText={handleChange('billing_average')}
              onBlur={handleBlur('billing_average')}
              style={{ color: themeContext.drawer.textProfileColor }}
              value={values.billing_average}
              error={touched.billing_average && errors.billing_average}
            />
          </FormItem>

          <FormItem>
            <Input
              numberOfLines={3}
              multiline
              style={{ color: themeContext.drawer.textProfileColor }}
              placeholder="O que você considera ponto forte na sua personalidade empreendedora?"
              onChangeText={handleChange('strongPoints')}
              onBlur={handleBlur('strongPoints')}
              value={values.strongPoints}
            />
          </FormItem>

          <FormItem>
            <Input
              numberOfLines={3}
              multiline
              style={{ color: themeContext.drawer.textProfileColor }}
              placeholder="Você possui alguma dúvida quanto empreendedor?"
              onChangeText={handleChange('entrepreneurDoubts')}
              onBlur={handleBlur('entrepreneurDoubts')}
              value={values.entrepreneurDoubts}
            />
          </FormItem>

          <FormItem>
            <Input
              numberOfLines={3}
              multiline
              style={{ color: themeContext.drawer.textProfileColor }}
              placeholder="Sobre você"
              onChangeText={handleChange('aboutMe')}
              onBlur={handleBlur('aboutMe')}
              value={values.aboutMe}
            />
          </FormItem>

          <FormItem>
            <View style={styles.row}>
              <CheckBox
                value={values.terms}
                onValueChange={newValue => setFieldValue('terms', newValue)}
                tintColors={{
                  false: themeContext.input.switch.darkMode,
                  true: themeContext.input.switch.lightMode,
                }}
              />
              <TouchableOpacity
                onPress={() => navigation.navigate('TermsOfUse')}
              >
                <Text style={styles.text}>
                  Eu li e concordo com os termos de uso
                </Text>
              </TouchableOpacity>
            </View>
            {touched.terms && errors.terms ? (
              <Text style={{ color: 'red' }}>{errors.terms}</Text>
            ) : null}
          </FormItem>

          <FormItem>
            <Button
              loading={loading}
              onPress={handleSubmit}
              size="large"
              disabled={loading || Object.keys(errors).length > 0 || !values.terms}
              text="Enviar"
              style={{ marginBottom: 44 }}
            />
          </FormItem>
        </Fragment>
      )}
    </Formik>
  );
};

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  text: {
    marginLeft: 8,
    textDecorationLine: 'underline',
  },
});
