import React, {useState} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  PermissionsAndroid,
  Platform,
  View,
  Text,
  Modal,
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import UserPicture from '../../components/LoggedUserPicture';
import {getStorageRef} from '../../services/storage';
import {useSelector} from 'react-redux';
import {notifyError} from '../../helpers/notifications';
import {updateUserService} from '../../services/users';

export default ({width, height}) => {
  const {user} = useSelector(state => state.auth);
  const [opened, setOpened] = useState(false);
  const [loading, setLoading] = useState(false);

  const requestPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: 'Permissão para usar a Câmera',
          message:
            'Este aplicativo precisa de acesso à sua câmera ' +
            'para que você possa tirar fotos incríveis.',
          buttonNeutral: 'Perguntar Depois',
          buttonNegative: 'Cancelar',
          buttonPositive: 'OK',
        },
      );
      return granted === PermissionsAndroid.RESULTS.GRANTED;
    } catch (err) {
      console.warn(err);
      return false;
    }
  };

  const permissionVerification = async () => {
    const hasCameraPermission = await requestPermission();
    if (!hasCameraPermission) {
      notifyError({
        title: 'Permissão Negada',
        message:
          'O aplicativo precisa de permissão para acessar a galeria e a câmera.',
      });
      return false;
    }
    return true;
  };

  const useGallery = async () => {
    if (Platform.OS === 'android') {
      const hasPermission = await permissionVerification();
      if (!hasPermission) {
        return;
      }
    }

    const image = await ImagePicker.openPicker({
      width: 500,
      height: 500,
      cropping: true,
      cropperToolbarTitle: 'Escolher imagem de Perfil',
      cropperCircleOverlay: true,
        mediaType: 'photo',
        includeBase64: true,
    });

    await saveImage(image);
  };

  const useCamera = async () => {
    const hasPermission = await permissionVerification();
    if (!hasPermission) {
      return;
    }

    const image = await ImagePicker.openCamera({
      width: 500,
      height: 500,
      cropping: true,
      cropperToolbarTitle: 'Escolher imagem de Perfil',
      cropperCircleOverlay: true,
        mediaType: 'photo',
        includeBase64: true,
    });

    await saveImage(image);
  };

  const saveImage = async image => {
    try {
      setOpened(false);
      setLoading(true);
      const storageRef = getStorageRef(`users/${user.id}/imageprofile.png`);
      await storageRef.putString(
        `data:${image.mime};base64,${image.data}`,
        'data_url',
      );
      const imageAddress = await storageRef.getDownloadURL();
      await updateUserService(user.id, {photoURL: imageAddress});
      setLoading(false);
    } catch (e) {
      setLoading(false);

      notifyError({
        title: 'Problema ao processar a imagem',
        message: 'Tente novamente mais tarde',
      });
    }
  };

  return (
    <>
      <TouchableOpacity onPress={() => setOpened(true)}>
        <UserPicture editIcon loading={loading} width={width} height={height} />
      </TouchableOpacity>
      <Modal
        transparent={true}
        visible={opened}
        onRequestClose={() => setOpened(false)}
        animationType="slide">
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <TouchableOpacity style={styles.optionButton} onPress={useCamera}>
              <Text style={styles.optionText}>Usar Câmera</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.optionButton} onPress={useGallery}>
              <Text style={styles.optionText}>Usar Galeria</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.cancelButton} onPress={() => setOpened(false)}>
              <Text style={styles.cancelText}>Cancelar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 20,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  optionButton: {
    paddingVertical: 15,
    alignItems: 'center',
  },
  optionText: {
    fontSize: 18,
    color: 'black',
  },
  cancelButton: {
    marginTop: 10,
    paddingVertical: 15,
    alignItems: 'center',
  },
  cancelText: {
    fontSize: 18,
    color: 'red',
  },
});
