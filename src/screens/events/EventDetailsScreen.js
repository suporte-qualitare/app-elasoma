import React, { useEffect, useState } from 'react';
import { Alert, ScrollView, View, TouchableOpacity, Platform } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import styled from 'styled-components/native';
import Header from '../../containers/Header';
import {
  createEventListenerService,
  updateEventService,
} from '../../services/events';
import ViewContainer from '../../components/Containers/ViewContainer';
import Text from '../../components/Typography/Text';
import EventHeader from '../../containers/events/EventHeader';
import Button from '../../components/Buttons/Button';
import { firestoreTimeToMoment, formattedDate } from '../../helpers/common';
import MapsStaticImage from '../../containers/events/MapsStaticImage';
import { Screens } from '../../contants/screens';
import { getUserFromStore } from '../../helpers/store';
import UserPicture from '../../components/UserPicture';
import TextButton from '../../components/Buttons/TextButton';
import { hasUserOnArrayOfObjects, removeUserFromArrayOfUsersObjects, userToObject } from '../../helpers/user';
import { catchError } from '../../helpers/errors';
import EventGallery from '../../containers/events/EventGallery';

// Função para remover campos undefined
const removeUndefinedFields = (obj) =>
  Object.fromEntries(Object.entries(obj).filter(([_, v]) => v !== undefined));

// Log helpers
const log = (message, data) => console.log(message, data);
const ConfirmedPeopleContainer = styled(View)`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-top: 40px;
`;

const DateText = styled(Text)`
  font-weight: 500;
  font-size: 18px;
  line-height: 43px;
`;

const AddressContainer = styled(View)`
  background-color: ${props => props.theme.colors.secondary};
  padding: 12px 24px;
  shadow-color: rgba(0, 0, 0, 0.08);
  margin-top: 10px;
  elevation: 3;
  shadow-opacity: 1;
  shadow-radius: 2;
  z-index: 1;
`;

const PlaceText = styled(Text)`
  font-weight: 700;
  font-size: 17px;
  line-height: 43px;
`;

const AddressText = styled(Text)`
  font-weight: 300;
  font-size: 15px;
  line-height: 17px;
`;

const DescriptionText = styled(Text)`
  font-family: Poppins;
  font-style: normal;
  font-weight: 300;
  font-size: 18px;
  line-height: 22px;
`;

const EventDetailsScreen = ({ route }) => {
  const navigation = useNavigation();
  const [loading, setLoading] = useState(true);
  const [loadingConfirmButton, setLoadingConfirmButton] = useState(false);
  const [event, setEvent] = useState(null);
  const user = getUserFromStore();

  const { id } = route.params;
  const userPresenceConfirmed = event?.confirmed?.some(person => person.id === user.id);
  const userDeniedConfirmed = event?.denied?.some(person => person.id === user.id);

  useEffect(() => {
    getEvent();
  }, []);

  const getEvent = async () => {
    try {
      setLoading(true);
      log('Fetching event with id:', id);
      createEventListenerService(id, (fetchedEvent) => {
        log('Event fetched successfully:', fetchedEvent);
        setEvent(fetchedEvent);
        setLoading(false);
      });
    } catch (error) {
      catchError(error);
      log('Error fetching event:', error);
      setLoading(false);
    }
  };
  const goToEventMapDetail = () => {
    navigation.push(Screens.EVENTS.navigator, {
      screen: Screens.EVENTS.EVENT_MAP_DETAILS_SCREEN,
      params: { event },
    });
  };

  const confirmPresence = async () => {
    try {
      setLoadingConfirmButton(true);
      log('Confirm presence clicked', { userPresenceConfirmed, event });

      const confirmed = event.confirmed?.length > 0 ? event.confirmed : [];
      const newConfirmed = hasUserOnArrayOfObjects(user, confirmed)
        ? removeUserFromArrayOfUsersObjects(user, confirmed)
        : [...confirmed, userToObject(user)];

      // Remover campos undefined
      const cleanConfirmed = newConfirmed.map(user => removeUndefinedFields(user));

      const newDenied = removeUserFromArrayOfUsersObjects(user, event.denied || []);
      const confirmedIds = cleanConfirmed.map(user => user.id);

      log('Updating event with new confirmed data:', { cleanConfirmed, newDenied, confirmedIds });

      await updateEventService(event.id, {
        confirmed: cleanConfirmed,
        denied: newDenied,
        deniedIds: newDenied.map(user => user.id),
        confirmedIds,
      });

      setEvent((prevEvent) => ({
        ...prevEvent,
        confirmed: cleanConfirmed,
        denied: newDenied,
        confirmedIds,
      }));

      log('Event updated successfully');
      setLoadingConfirmButton(false);
    } catch (error) {
      catchError(error, 'EventDetailsScreen->confirmPresence');
      log('Error confirming presence:', error);
      setLoadingConfirmButton(false);
    }
  };

  const confirmDeny = async () => {
    Alert.alert(
      'Você tem certeza que não irá participar desse evento?',
      '',
      [
        { text: 'Não', style: 'cancel' },
        { text: 'Sim', onPress: denyPresence }
      ],
      { cancelable: false }
    );
  };

  const denyPresence = async () => {
    try {
      const denied = event.denied?.length > 0 ? event.denied : [];
      const newDenied = hasUserOnArrayOfObjects(user, denied)
        ? removeUserFromArrayOfUsersObjects(user, denied)
        : [...denied, userToObject(user)];
      const deniedIds = newDenied.map(user => user.id);

      log('Updating event with new denied data:', { newDenied, deniedIds });

      await updateEventService(event.id, {
        denied: newDenied,
        deniedIds,
      });

      setEvent((prevEvent) => ({
        ...prevEvent,
        denied: newDenied,
        deniedIds,
      }));

      log('Presence denial updated successfully');
    } catch (error) {
      catchError(error, 'EventDetailsScreen->confirmDeny');
      log('Error denying presence:', error);
    }
  };

  const ConfirmedPeople = () => {
    const { dateTime } = event;
    const date = formattedDate(
      firestoreTimeToMoment(dateTime),
      'DD MMM - HH:mm',
    );

    return (
      <ConfirmedPeopleContainer
        style={
          Platform.OS === 'ios' ? { marginTop: 60, alignItems: 'center' } : { marginTop: 30, alignItems: 'center' }
        }
      >
        <DateText>{date.toUpperCase()}</DateText>
        <EventConfirmedPeople people={event.confirmed} />
      </ConfirmedPeopleContainer>
    );
  };

  const getMainButtonText = () => {
    if (event.happened) return 'Evento Encerrado';
    if (userPresenceConfirmed) return 'Remover Presença';
    if (userDeniedConfirmed) return 'Não vou participar';
    return 'Confirmar Presença';
  };

  const handleMainButtonPress = () => {
    if (event.happened) return;
    if (userPresenceConfirmed) {
      confirmPresence(); // Call the same function to toggle presence
    } else {
      confirmPresence();
    }
  };

  return (
    <>
      <Header />
      <ViewContainer noPaddingHorizontal loading={loading}>
        {event && (
          <ScrollView scrollIndicatorInsets={{ right: Number.MIN_VALUE }}>
            <CenteredView>
              <View>
                <EventHeader event={event} />
              </View>
            </CenteredView>

            <ViewContainer style={{ paddingTop: 32 }}>
              <CenteredView>
                <Button
                  disabled={event.happened}
                  loading={loadingConfirmButton}
                  onPress={handleMainButtonPress}
                  size="medium"
                  text={getMainButtonText()}
                />
                {!event.happened && !userPresenceConfirmed && !userDeniedConfirmed && (
                  <TextButton onPress={confirmDeny} underline text="Não vou participar" />
                )}
              </CenteredView>
              <ConfirmedPeople style={{ marginTop: 15 }} />
            </ViewContainer>
            <EventGallery event={event} />
            <AddressContainer paddingVertical={12}>
              <PlaceText>{event.place}</PlaceText>
              <AddressText>{event.address}</AddressText>
            </AddressContainer>
            <View style={{ zIndex: 10 }}>
              <TouchableOpacity onPress={goToEventMapDetail}>
                <MapsStaticImage
                  height={160}
                  mapCoordinates={event.mapCoordinates}
                  address={event.address}
                />
              </TouchableOpacity>
            </View>
            <ViewContainer paddingVertical={24}>
              <DescriptionText>{event.description}</DescriptionText>
            </ViewContainer>
          </ScrollView>
        )}
      </ViewContainer>
    </>
  );
};

const EventConfirmedPeople = ({ people }) => {
  const navigation = useNavigation();

  const goToEventsConfirmedPeopleScreen = () => {
    navigation.push(Screens.EVENTS.navigator, {
      screen: Screens.EVENTS.EVENT_CONFIRMED_PEOPLE_SCREEN,
      params: { people },
    });
  };

  if (people) {
    const shown = people.slice(0, 3);
    return (
      <TouchableOpacity onPress={goToEventsConfirmedPeopleScreen}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          {shown.map(show => (
            <UserPicture
              key={show.id}
              style={{ marginLeft: 4 }}
              photoURL={show.photoURL}
              height={24}
              width={24}
            />
          ))}
          {people.length > 3 ? (
            <Text style={{ marginLeft: 4 }}>
              {people.length > 0 && `+${people.length - 3}`}
            </Text>
          ) : null}
        </View>
      </TouchableOpacity>
    );
  }
  return null;
};

export default EventDetailsScreen;

// Styled Components
const CenteredView = styled(View)`
  align-items: center;
  justify-content: center;
`;
